from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY

import json, requests

def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "query": f'{city}, {state}',
        "per_page": "1"
    }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, headers=headers, params=params)
    content = response.json()

    try:    
        return {"picture_url": content["photos"][0]["src"]["original"]} 
    except(KeyError, IndexError, requests.exceptions.JSONDecodeError):
        return {"picture_url": None}

def get_weather_data(city, state):
    url = f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(url)
    content = response.json()
    print(content)
    
    lat = content[0]["lat"]
    lon = content[0]["lon"]
    weather_url = f"http://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}&units=imperial"
    response = requests.get(weather_url)
    weather_data = response.json()
    print(weather_data)
    temp = weather_data["main"]["temp"]
    description = weather_data["weather"][0]["description"]
    return {
        "temp": temp,
        "description": description,
    }
